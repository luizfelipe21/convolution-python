from tkinter import *
from tkinter import messagebox
import numpy as np
from PIL import Image, ImageTk
import matplotlib.pyplot as plt

bg = 'cyan'
fonte12 = ('Comic Sans MS', 12, 'bold')
fonte14 = ('Comic Sans MS', 14, 'bold')
fonte18 = ('Impact', 20, 'bold')


class Convolucao(object):
    def __init__(self):
        self.i = Tk()
        self.i.title('Convolucao')
        self.i['bg'] = bg
        self.i.resizable('False', 'False')

        # imagem em que a convolução será efetuada
        self.nome = 'sans.png'
        self.mode = 'L'
        self.animacao = False

        self.tela = Frame(self.i, bg=bg)
        self.tela.grid(row=0, column=0, sticky=S)
        self.frame_bts = Frame(self.tela, bg=bg)
        self.frame_entries = Frame(self.tela, bg=bg)
        self.Fimg = Frame(self.i, bg=bg)

        img = Image.open(self.nome).convert(self.mode)
        self.img = np.asarray(img)
        img = ImageTk.PhotoImage(img.resize((self.img.shape[1] * 2, self.img.shape[0] * 2)))
        self.c_imagem = Canvas(self.Fimg, width=self.img.shape[1] * 2, height=self.img.shape[0] * 2, bg=bg)
        self.c_imagem.create_image(self.img.shape[1], self.img.shape[0], image=img)
        self.check = Checkbutton(self.Fimg, text='Animacao', font=fonte12, bg=bg, command=self.anima)
        self.bt_tratamento = Button(self.Fimg, text='Tratamento da imagem', width=20, command=self.varrer_imagem,
                                    font=fonte12, bg='pink')
        self.d = Button(self.Fimg, text='?', font=fonte18, bg='pink', fg='dark green', width=4, command=self.box)

        self.bt_confirm = Button(self.tela, text='Customizar', font=fonte12, bg='pink', width=20
                                 , command=self.aplica_custom)

        self.bt_sharpen = Button(self.frame_bts, text='Sharpen', font=fonte12, width=20, command=self.aplica_sharpen
                                 , bg='pink')
        self.bt_dectbordas = Button(self.frame_bts, text='Deteccao de bordas', font=fonte12, width=20
                                    , command=self.aplica_dec_bordas, bg='pink')
        self.bt_gaus = Button(self.frame_bts, text='Gaussian', font=fonte12, width=20, command=self.aplica_gaus
                              , bg='pink')
        self.bt_shift = Button(self.frame_bts, text='Shift', font=fonte12, width=20, command=self.aplica_shift
                               , bg='pink')
        self.bt_bigrect = Button(self.frame_bts, text='Media', font=fonte12, width=20, command=self.aplica_bigrect
                                 , bg='pink')
        self.bt_shake = Button(self.frame_bts, text='Hand Shake', font=fonte12, width=20, command=self.aplica_shake
                               , bg='pink')

        self.bt_dectbordas.grid()
        self.bt_sharpen.grid(row=1)
        self.bt_gaus.grid(row=0, column=1)
        self.bt_shift.grid(row=1, column=1)
        self.bt_bigrect.grid(row=2, column=0)
        self.bt_shake.grid(row=2, column=1)

        self.e1 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e2 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e3 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e4 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e5 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e6 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e7 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e8 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e9 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e10 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e11 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e12 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e13 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e14 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e15 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e16 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e17 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e18 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e19 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e20 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e21 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e22 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e23 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e24 = Entry(self.frame_entries, width=5, font=fonte12)
        self.e25 = Entry(self.frame_entries, width=5, font=fonte12)
        self.entrie_pack()
        self.aplica_bigrect()

        self.Fimg.grid(row=0, column=1)
        Label(self.Fimg, text='Imagem selecionada', font=fonte12, bg=bg).grid(row=0, column=0)
        self.c_imagem.grid(row=1)
        self.bt_tratamento.grid(row=2)
        self.check.grid(row=3)
        self.d.grid(row=5, column=0)

        Label(self.tela, bg=bg, font=fonte18, text='Convolution App').pack()
        Label(self.tela, text='Nucleos', font=fonte12, bg=bg).pack()
        self.frame_bts.pack()
        Label(self.tela, text='Nucleo customizado', font=fonte12, bg=bg).pack()
        self.frame_entries.pack()
        self.err = Label(self.tela, font=fonte12, bg=bg, fg='red')
        self.err.pack()
        self.bt_confirm.pack()

        self.i.mainloop()

    def anima(self):
        self.animacao = not self.animacao

    def aplica_custom(self):
        try:
            self.err['text'] = ''
            self.funcao = np.array([
                [float(self.e1.get()), float(self.e2.get()), float(self.e3.get()), float(self.e4.get()),
                 float(self.e5.get())],
                [float(self.e6.get()), float(self.e7.get()), float(self.e8.get()), float(self.e9.get()),
                 float(self.e10.get())],
                [float(self.e11.get()), float(self.e12.get()), float(self.e13.get()), float(self.e14.get()),
                 float(self.e15.get())],
                [float(self.e16.get()), float(self.e17.get()), float(self.e18.get()), float(self.e19.get()),
                 float(self.e20.get())],
                [float(self.e21.get()), float(self.e22.get()), float(self.e23.get()), float(self.e24.get()),
                 float(self.e25.get())]])
            self.pega_valores()
        except:
            self.err['text'] = 'Nucleo invalido'

    def aplica_sharpen(self):
        self.funcao = np.array([[0.00, 0.00, 0.00, 0.00, 0.00],
                                [0.00, -1, -1, -1, 0.00],
                                [0.00, -1, 9, -1, 0.00],
                                [0.00, -1, -1, -1, 0.00],
                                [0.00, 0.00, 0.00, 0.00, 0.00]])
        self.pega_valores()

    def aplica_gaus(self):
        self.funcao = np.array([[0.01, 0.02, 0.03, 0.02, 0.01],
                                [0.02, 0.06, 0.08, 0.06, 0.02],
                                [0.03, 0.08, 0.11, 0.08, 0.03],
                                [0.02, 0.06, 0.08, 0.06, 0.02],
                                [0.01, 0.02, 0.03, 0.02, 0.01]])
        self.pega_valores()

    def aplica_dec_bordas(self):
        self.funcao = np.array([[0.0, 0.0, 0.0, 0.0, 0.0],
                                [0.0, 1.0, 1.0, 1.0, 0.0],
                                [0.0, 1.0, -8, 1.0, 0.0],
                                [0.0, 1.0, 1.0, 1.0, 0.0],
                                [0.0, 0.0, 0.0, 0.0, 0.0]])
        self.pega_valores()

    def aplica_shift(self):
        self.funcao = np.array([[0.0, 0.0, 0.0, 0.0, 0.0],
                                [0.0, 0.0, 0.0, 0.0, 0.0],
                                [1.0, 0.0, 0.0, 0.0, 0.0],
                                [0.0, 0.0, 0.0, 0.0, 0.0],
                                [0.0, 0.0, 0.0, 0.0, 0.0]])
        self.pega_valores()

    def aplica_shake(self):
        self.funcao = np.array([[0.2, 0.0, 0.0, 0.0, 0.0],
                                [0.0, 0.2, 0.0, 0.0, 0.0],
                                [0.0, 0.0, 0.2, 0.0, 0.0],
                                [0.0, 0.0, 0.0, 0.2, 0.0],
                                [0.0, 0.0, 0.0, 0.0, 0.2]])
        self.pega_valores()

    def aplica_bigrect(self):
        self.funcao = np.array([[0.04, 0.04, 0.04, 0.04, 0.04],
                                [0.04, 0.04, 0.04, 0.04, 0.04],
                                [0.04, 0.04, 0.04, 0.04, 0.04],
                                [0.04, 0.04, 0.04, 0.04, 0.04],
                                [0.04, 0.04, 0.04, 0.04, 0.04]])
        self.pega_valores()

    def varrer_imagem(self):
        try:
            aux = Image.open(self.nome).convert(self.mode)
            for y in range(self.img.shape[0]):
                for x in range(self.img.shape[1]):
                    aux.putpixel((x, y), self.convolucao(x, y))
                    if self.animacao:
                        img = ImageTk.PhotoImage(aux.resize((self.img.shape[1] * 2, self.img.shape[0] * 2)))
                        self.c_imagem.create_image(self.img.shape[1], self.img.shape[0], image=img)
                        self.c_imagem.update()

            plt.subplot(1, 2, 1)
            plt.imshow(self.img, cmap='gray')
            plt.title('Imagem original')
            plt.subplot(1, 2, 2)
            plt.imshow(aux, cmap='gray')
            plt.title('Imagem com filtro')
            plt.show()
        except:
            self.err['text'] = 'Nucleo invalido!'

    def convolucao(self, x, y):
        if y < 2 or x < 2:
            return 0
        aux = np.zeros((5, 5), dtype=int)
        r = y - 2
        c = x - 2
        try:
            for i in range(5):
                for j in range(5):
                    aux[i, j] = self.img[r + i, c + j]
            a = int(sum(sum(self.funcao * aux)))
            return a
        except:
            return 0

    def box(self):
        messagebox.showinfo('Funcionamento'
                            , message='A convolução é uma operação matemática geralmente usada em processamento '
                                      'de imagens. Nessa operação, pegamos o valor de cada pixel de uma imagem e '
                                      'efetuamos uma média ponderada com um núcleo, gerando uma nova matriz '
                                      '(imagem de saída)')

    def pega_valores(self):
        self.e1.delete(0, END), self.e1.insert(END, self.funcao[0][0])
        self.e2.delete(0, END), self.e2.insert(END, self.funcao[0][1])
        self.e3.delete(0, END), self.e3.insert(END, self.funcao[0][2])
        self.e4.delete(0, END), self.e4.insert(END, self.funcao[0][3])
        self.e5.delete(0, END), self.e5.insert(END, self.funcao[0][4])
        self.e6.delete(0, END), self.e6.insert(END, self.funcao[1][0])
        self.e7.delete(0, END), self.e7.insert(END, self.funcao[1][1])
        self.e8.delete(0, END), self.e8.insert(END, self.funcao[1][2])
        self.e9.delete(0, END), self.e9.insert(END, self.funcao[1][3])
        self.e10.delete(0, END), self.e10.insert(END, self.funcao[1][4])
        self.e11.delete(0, END), self.e11.insert(END, self.funcao[2][0])
        self.e12.delete(0, END), self.e12.insert(END, self.funcao[2][1])
        self.e13.delete(0, END), self.e13.insert(END, self.funcao[2][2])
        self.e14.delete(0, END), self.e14.insert(END, self.funcao[2][3])
        self.e15.delete(0, END), self.e15.insert(END, self.funcao[2][4])
        self.e16.delete(0, END), self.e16.insert(END, self.funcao[3][0])
        self.e17.delete(0, END), self.e17.insert(END, self.funcao[3][1])
        self.e18.delete(0, END), self.e18.insert(END, self.funcao[3][2])
        self.e19.delete(0, END), self.e19.insert(END, self.funcao[3][3])
        self.e20.delete(0, END), self.e20.insert(END, self.funcao[3][4])
        self.e21.delete(0, END), self.e21.insert(END, self.funcao[4][0])
        self.e22.delete(0, END), self.e22.insert(END, self.funcao[4][1])
        self.e23.delete(0, END), self.e23.insert(END, self.funcao[4][2])
        self.e24.delete(0, END), self.e24.insert(END, self.funcao[4][3])
        self.e25.delete(0, END), self.e25.insert(END, self.funcao[4][4])

    def entrie_pack(self):
        self.e1.grid(row=0, column=0)
        self.e2.grid(row=0, column=1)
        self.e3.grid(row=0, column=2)
        self.e4.grid(row=0, column=3)
        self.e5.grid(row=0, column=4)
        self.e6.grid(row=1, column=0)
        self.e7.grid(row=1, column=1)
        self.e8.grid(row=1, column=2)
        self.e9.grid(row=1, column=3)
        self.e10.grid(row=1, column=4)
        self.e11.grid(row=2, column=0)
        self.e12.grid(row=2, column=1)
        self.e13.grid(row=2, column=2)
        self.e14.grid(row=2, column=3)
        self.e15.grid(row=2, column=4)
        self.e16.grid(row=3, column=0)
        self.e17.grid(row=3, column=1)
        self.e18.grid(row=3, column=2)
        self.e19.grid(row=3, column=3)
        self.e20.grid(row=3, column=4)
        self.e21.grid(row=4, column=0)
        self.e22.grid(row=4, column=1)
        self.e23.grid(row=4, column=2)
        self.e24.grid(row=4, column=3)
        self.e25.grid(row=4, column=4)


Convolucao()
